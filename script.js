
/* Get data objects */
function getDraftDataObject() {
  return {
    groups: [],
    selectedGroupInternal: null,
    set selectedGroup(val) {
      this.selectedGroupInternal = this.groups.find(group => group.name === val);

      this.selectedGroupInternal.calculateAverageGrade();
      
      // Refresh sidebar
      this.populateGroupView();
      this.selectedGroupInternal.populateStudentsList();

      // Refresh content
      if (this.selectedStudent) setupForm();
      else if (this.selectedGroupInternal.students.length) {
        this.selectedGroupInternal.selectedStudent = this.selectedGroupInternal.students[0].name;
      }
      else {
        contentHeader.innerHTML = 'Add some students!';
        contentForm.innerHTML = '';
      }
    },
    get selectedGroup() {
      return this.selectedGroupInternal;
    },
    get selectedStudent() {
      return this.selectedGroup.selectedStudent;
    },
    addGroup(name, doNotSave) {
      try {
        if (!name) return false;
        if (this.groups.find(group => group.name === name)) return false;
    
        const newGroup = getNewGroupObject(name);
        this.groups.push(newGroup);
        this.selectedGroup = name;
        if (contentHeader.innerHTML = 'Create a group first!') contentHeader.innerHTML = 'Add some students!';
  
        if (!doNotSave) saveToLocalStorage();
      }
      catch(err) {
        showError(err);
      }
    },
    deleteGroup(name) {
      try {
        if (!name) return false;
    
        const indexToDelete = this.groups.findIndex(group => group.name === name);
        if (indexToDelete < 0) return false;
  
        this.groups.splice(indexToDelete, 1);
  
        // Refresh
        this.selectedGroup.students.length = 0;
        this.selectedGroup.populateStudentsList();
        this.populateGroupView();
  
        // Add a group if we're out of one, else select other group
        if (!this.groups.length) {
          this.selectedGroup.deselectStudent();
          contentHeader.innerHTML = 'Create a group first!';
          contentForm.innerHTML = '';
  
          toggleDisplayAddGroupModal();
        }
        else this.selectedGroup = this.groups[0].name;
  
        saveToLocalStorage();
      }
      catch(err) {
        showError(err);
      }
    },
    populateGroupView() {
      let selectGroupHTML = '';
      this.groups.forEach((group) => {
        if (group.name === this.selectedGroup.name) selectGroupHTML = selectGroupHTML + `<option selected>${group.name}</option>`;
        else selectGroupHTML = selectGroupHTML + `<option>${group.name}</option>`;
      });
      selectGroup.innerHTML = selectGroupHTML;
  
      if (this.selectedGroup.students.length) {
        const totalStudents = this.selectedGroup.students.length;
        const numberGraded = this.selectedGroup.students.reduce((acc, student) => typeof student.grade === 'number' ? acc + 1 : acc, 0);
        graded.innerHTML = `${numberGraded}/${totalStudents}`;
        average.innerHTML = this.selectedGroup.stats.averageGrade;
        percentagePass.innerHTML = this.selectedGroup.stats.percentagePass !== null ? this.selectedGroup.stats.percentagePass + '%' : '';
      }
      else {
        graded.innerHTML = '0/0';
        average.innerHTML = '';
      }
    },
    restoreGroups(names) {
      /* Restore groups from locally saved data */
      try {
        names.forEach((name) => this.addGroup(name, true));
      }
      catch(err) {
        showError(err);
        restoreError = true;
      }
    },
  }
}

function getNewGroupObject(name) {
  const escapedName = name.replace(/["']/g, "`");
  const draftGroupObject = {
    name: escapedName,
    students: [],
    stats: {
      averageGrade: null,
      averageGradeIncludingZeroes: null,
      percentagePass: null,
    },
    selectedStudentInternal: null,
    set selectedStudent(val) {
      this.selectedStudentInternal = this.students.find(student => student.name === val);

      this.populateStudentsList();
      setupForm();
    },
    get selectedStudent() {
      return this.selectedStudentInternal;
    },
    deselectStudent() {
      this.selectedStudentInternal = null;
    },
    calculateAverageGrade() {
      // Just the average for block 5 (no distinction between with or without zeroes as we don't have to check non submissions)
      let numberOfGrades = this.students.filter(student => typeof student.grade === 'number').length;
      if (!numberOfGrades) this.stats.averageGrade = null;
      let gradeTotal = this.students.reduce((acc, student) => typeof student.grade === 'number' ? acc + student.grade : acc, 0);
      this.stats.averageGrade = Math.round((gradeTotal / numberOfGrades) * 100) / 100 || null;

      // Then the same excluding zeroes
      // numberOfGrades = this.students.filter(student => student.grade).length;
      // if (!numberOfGrades) this.stats.averageGrade = null;
      // gradeTotal = this.students.reduce((acc, student) => student.grade ? acc + student.grade : acc, 0);
      // this.stats.averageGrade = Math.round((gradeTotal / numberOfGrades) * 100) / 100 || null;
      
      // Calculate percentage pass
      numberOfGrades = this.students.filter(student => student.grade).length;
      if (!numberOfGrades) this.stats.averageGrade = null; else {
        let passTotal = this.students.reduce((acc, student) => student.grade >= 5.5 ? acc + 1 : acc, 0);
        this.stats.percentagePass = Math.round((passTotal / numberOfGrades) * 100);
      }
    },
    addStudent(name, doNotSave) {
      try {
        if (!name) return false;
        if (this.students.find(student => student.name === name)) return false;
    
        const newStudent = getNewStudentObject(name);
        this.students.push(newStudent);
  
        this.populateStudentsList();
        data.populateGroupView();
  
        if (this.students.length === 1) this.selectedStudent = name;
        if (!doNotSave) saveToLocalStorage();
      }
      catch(err) {
        showError(err);
      }
    },
    deleteStudent(name) {
      try {
        const deleteIndex = this.students.findIndex(student => student.name === name);
        if (deleteIndex < 0) return false;
  
        this.students.splice(deleteIndex, 1);
  
        // Select another student (if this one was selected)
        if (this.selectedStudent && this.selectedStudent.name === name && !this.students.length) {
          contentHeader.innerHTML = 'Add some students!';
          contentForm.innerHTML = '';
          this.deselectStudent();
        }
        else if (this.selectedStudent && this.selectedStudent.name === name) {
          this.selectedStudent = this.students[0].name;
        }
  
        this.calculateAverageGrade();
        this.populateStudentsList();
        data.populateGroupView();
        saveToLocalStorage();
      }
      catch(err) {
        showError(err);
      }
    },
    populateStudentsList() {
      this.students.sort((a, b) => {
        const nameA = a.name.toUpperCase().split(' ');
        const nameB = b.name.toUpperCase().split(' ');
        const surnameA = nameA[nameA.length-1];
        const surnameB = nameB[nameB.length-1];
        if (surnameA < surnameB) return -1;
        if (surnameA > surnameB) return 1;
        return 0;
      });

      let studentsListHTML = '';
      this.students.forEach(student => {
        let selectedClass = '';
        if (this.selectedStudent && student.name === this.selectedStudent.name) selectedClass = ' students-list-item-selected';
        studentsListHTML = studentsListHTML + `
<div class="students-list-item${selectedClass}" onClick="selectStudent('${student.name}')">
  <div id="name-${student.name}">
    ${student.name}
  </div>
  <div class="students-list-item-right">
    <div class="delete-student" onClick="deleteStudent(event, '${student.name}')">
      x
    </div>
    <div id="grade-${student.name}">
      ${typeof student.grade === 'number' ? student.grade : '/'}
    </div>
  </div>
</div>`;
      });

      studentsList.innerHTML = studentsListHTML;
    },
    restoreStudents(students) {
      /* Restore name, categories, generalComment, and grade from locally stored data */
      try {
        students.forEach((student) => {
          // Find existing student (to replace values) or create a new one
          let currentStudent = this.students.find(currentStudent => currentStudent.name === student.name);
          if (!currentStudent) {
            this.addStudent(student.name, true);
            currentStudent = this.students.find(currentStudent => currentStudent.name === student.name);
          }

          const doubleQuotationIsFixed = localStorage.getItem('doubleQuotationIsFixed');
          if (!doubleQuotationIsFixed) {
            // We need to replace " with a double ', else the csv export doesn't work properly.
            // Only needs to be done once for existing comments, new comments are now properly handled (see commit 929eff4).
            if (student.generalComment) student.generalComment = student.generalComment.replace(/"/g, "''");
            student.categories.forEach((category) => {
              category.comments.forEach((comment, index) => {
                if (comment) category.comments[index] = comment.replace(/"/g, "''");
              });
              localStorage.setItem('doubleQuotationIsFixed', true);
            });
          }
  
          currentStudent.grade = student.grade;
          currentStudent.generalComment = student.generalComment;
          currentStudent.categories = student.categories;
        });
        this.populateStudentsList();
      }
      catch(err) {
        showError(err);
        restoreError = true;
      }
    },
  };

  return draftGroupObject;
}

function getNewStudentObject(name) {
  const escapedName = name.replace(/["']/g, "`");
  const draftStudentObject = {
    name: escapedName,
    grade: null,
    generalComment: null,
    calculateGrade() {
      const essayHadNoGradeYet = !this.grade;

      const allCategoriesHaveScores = this.categories.every(category => typeof category.score === 'number');
      if (!allCategoriesHaveScores) {
        this.grade = null;
        data.selectedGroup.calculateAverageGrade();
        return false;
      }
  
      const score = this.categories.reduce((accumulativeScore, category) => accumulativeScore + category.score, 0);
      this.grade = score / 10;

      data.selectedGroup.calculateAverageGrade();
      data.selectedGroup.populateStudentsList();
      data.populateGroupView();

      if (essayHadNoGradeYet) checkDisplayExportReminder();
    },
    setScore({ category, score }) {
      try {
        const catObj = this.categories.find(cat => cat.name === category);
        if (score === null) catObj.score = null;
        else catObj.score = Math.min(score, catObj.maxScore);

        this.calculateGrade();
        saveToLocalStorage(true);
      }
      catch(err) {
        showError(err);
      }
    },
    setComment({ category, index, comment }) {
      try {
        const catObj = this.categories.find(cat => cat.name === category);
        catObj.comments[index] = comment;
  
        saveToLocalStorage(true);
      }
      catch(err) {
        showError(err);
      }
    },
    categories: [
      {
        name: 'formal',
        score: null,
        maxScore: 10,
        comments: [],
      },
      {
        name: 'organization',
        score: null,
        maxScore: 15,
        comments: [],
      },
      {
        name: 'representation',
        score: null,
        maxScore: 20,
        comments: [],
      },
      {
        name: 'argumentation',
        score: null,
        maxScore: 30,
        comments: [],
      },
      {
        name: 'language',
        score: null,
        maxScore: 10,
        comments: [],
      },
      {
        name: 'beyond',
        score: null,
        maxScore: 15,
        comments: [],
      },
    ],
  };

  return draftStudentObject;
}

/* Saved comments */
const savedCommentsData = JSON.parse(localStorage.getItem('savedComments')) || [];

// little helper function
function hasClass(elem, className) {
  return elem.className.split(' ').indexOf(className) > -1;
}

function toggleSavedComments() {
  const savedCommentsAreShown = savedCommentsContainer.style.display === 'block';
  if (savedCommentsAreShown) {
    overlay.style.display = 'none';
    savedCommentsContainer.style.display = 'none';
  }
  else {
    overlay.style.display = 'block';
    savedCommentsContainer.style.display = 'block';

    overlay.addEventListener('click', function (e) {
      if (hasClass(e.target, 'overlay')) {
        if (editingSavedComment || editingSavedComment === 0) saveNewComment();

        toggleSavedComments();
      }
    },
    {
      once: true,
    });
  }
}

function toggleNewComment() {
  const newCommentBoxIsShown = Number(newComment.style.maxHeight.slice(0, -2)) > 0;
  if (newCommentBoxIsShown) {
    newComment.style.maxHeight = '0px';
    addNewSavedCommentButton.innerText = 'Add comment';
    savedComments.style.maxHeight = '72vh';

    // Editing a saved comment is by deleting it and then adding a new one. Hence, cancelling an edit should re-add the old one
    // if (editingSavedComment || editingSavedComment === 0) saveNewComment();

    // Reset inputs
    newCommentTitle.value = null;
    newCommentBody.value = null;
  }
  else {
    newComment.style.maxHeight = newComment.scrollHeight + 'px';
    addNewSavedCommentButton.innerText = 'Cancel';
    savedComments.style.maxHeight = '48vh';
  }
}

function populateSavedComments() {
  let innerHTML = '';

  if (!savedCommentsData.length) {
    // No saved comments yet
    innerHTML = 'No saved comments!';
  }
  else {
    savedCommentsData.forEach((comment, index) => {
      innerHTML += `
        <div class="saved-comment" id="saved-comment-${index}">
          <div class="saved-comment-title">
            ${comment.title}
          </div>
          <div class="saved-comment-body" id="saved-comment-${index}-body">
            ${comment.body}
          </div>
          <div class="saved-comment-actions">
            <div onclick="copySavedComment(${index})">Copy comment to clipboard</div>
            <div onclick="editSavedComment(${index})">Edit</div>
            <div onclick="deleteSavedComment(${index})">Delete</div>
          </div>
        </div>
      `
    });
  }

  savedComments.innerHTML = innerHTML;
}

// Stores the index of the comment we're editing and serves as indicator
let editingSavedComment = false;
function editSavedComment(index) {
  // Strategy: comments gets re-added, at that moment the old comment gets deleted
  editingSavedComment = index;
  toggleNewComment();
  newCommentTitle.value = savedCommentsData[index].title;
  newCommentBody.value = savedCommentsData[index].body;
}

function deleteSavedComment(index, force) {
  if (force || confirm('Sure you want to delete this comment?')) {
    savedCommentsData.splice(index, 1);
    sortSavedComments();
    populateSavedComments();
    localStorage.setItem('savedComments', JSON.stringify(savedCommentsData));
  }
}

function saveNewComment() {
  const reAddingFromEdit = !!editingSavedComment || editingSavedComment === 0;

  const newCommentObj = {
    title: newCommentTitle.value,
    body: newCommentBody.value,
  };
  savedCommentsData.push(newCommentObj);

  if (reAddingFromEdit) {
    deleteSavedComment(editingSavedComment, true);
    editingSavedComment = null;
    const newCommentBoxIsShown = Number(newComment.style.maxHeight.slice(0, -2)) > 0;
    if (newCommentBoxIsShown) toggleNewComment();
  }
  else {
    // In an else clause since deleteSavedComment also executes this stuff
    sortSavedComments();
    populateSavedComments();
    localStorage.setItem('savedComments', JSON.stringify(savedCommentsData));
  }
}

function sortSavedComments() {
  savedCommentsData.sort((a, b) => {
    if (a.title.toUpperCase() > b.title.toUpperCase()) return 1;
    if (a.title.toUpperCase() < b.title.toUpperCase()) return -1;
    return 0;
  });
}

function copySavedComment(index) {
  const commentBody = document.getElementById(`saved-comment-${index}-body`).textContent.trim();
  copyBox.value = commentBody;
  copyBox.focus();
  copyBox.select();
  if (!document.execCommand('copy')) {
    // Copying went wrong...
    showNotification({ message: 'Oops, could not copy that!', duration: 2000 });
  }
  else showNotification({ message: 'Copied!', duration: 1000 });
}

/* Helper functions for CRUD and select operations */
function createGroup() {
  const name = modalNewGroupInput.value;
  modalNewGroupInput.value = '';
  data.addGroup(name);
  toggleDisplayAddGroupModal();
}

function deleteGroup() {
  const name = data.selectedGroup.name;
  if (confirm(`Deleting group '${name}' cannot be undone. Sure?`)) {
    data.deleteGroup(name);
  }
}

function addStudent() {
  const name = addStudentInput.value;
  addStudentInput.value = '';
  
  if (data.selectedGroup) data.selectedGroup.addStudent(name);
  else {
    alert('Please create or select a group first!');
  }
}

function toggleStudentList(button) {
  if (!data.groups.length) {
    alert('Plese create or select a group first!');
    return;
  }

  if (addStudentList.style.display != 'flex') {
    addStudentList.style.display = 'flex';
    button.innerHTML = 'Click to submit';
  } else {
    newStudents = addStudentList.value.split("\n");
    for(var i = 0; i < newStudents.length; i++) {
      data.selectedGroup.addStudent(newStudents[i]);
    }
    addStudentList.value = '';
    addStudentList.style.display = 'none';
    button.innerHTML = 'Add student list';
  }
}

function selectStudent(name) {
  data.selectedGroup.selectedStudent = name;
}

function deleteStudent(event, name) {
  event.stopPropagation();
  if (confirm(`Are you sure you want to delete student '${name}? This cannot be undone.`)) {
    data.selectedGroup.deleteStudent(name);
  }
}

function switchGroups() {
  data.selectedGroup = selectGroup.value;
}

function updateComment({ target }, { category, index }) {
  try {
    data.selectedGroup.selectedStudent.setComment({
      category,
      index,
      comment: target.value.replace(/"/g, "''"),
    });
  }
  catch(err) {
    showError(err);
  }
}

function updateGeneralComment({ target }) {
  try {
    data.selectedGroup.selectedStudent.generalComment = target.value.replace(/"/g, "''");
    saveToLocalStorage(true);
  }
  catch(err) {
    showError(err);
  }
}

function updateScore({ target }, category) {
  try {
    const { score: storedScore } = data.selectedGroup.selectedStudent.categories.find(cat => cat.name === category);

    let score = target.value;
    if (score === '') score = null;
    else score = Number(score);

    if (Number.isNaN(score) && score !== null) {
      target.value = storedScore;
      return;
    }
  
    const maxScores = {
      'formal': 10,
      'organization': 15,
      'representation': 20,
      'argumentation': 30,
      'language': 10,
      'beyond': 15,
    };
  
    if (score < 0 || score > maxScores[category]) {
      alert('That is outside the range for this category!');
      target.value = storedScore;
      return;
    }
  
    data.selectedGroup.selectedStudent.setScore({
      category,
      score,
    });
  
    if (score) document.getElementById(`${category}-grade`).innerHTML = ` (${Math.round(score / maxScores[category] * 100) / 10} / 10)`;
    else document.getElementById(`${category}-grade`).innerHTML = ' (... / 10)';
    data.populateGroupView();
    data.selectedGroup.populateStudentsList();
  }
  catch(err) {
    showError(err);
  }
}

/* Create content and handle category (de)collapsing */
function setupForm() {
  const student = data.selectedGroup.selectedStudent;
  if (!student) return;
  const innerHTML = `
  <span class="general-comment-header">
    General comment
  </span>
  <textarea onblur="updateGeneralComment(event)" class="general-comment">${student.generalComment || ''}</textarea>
  <div class="category">
    <input type="radio" name="toggle-category" id="formal" class="toggle-radio" />
    <div class="category-header" id="cat-header-formal" onmousedown="verifyRadioButtonAction('formal')" onclick="resetScroll('formal')">
      <label for="formal" class="label">
        Formal requirements & completeness
      </label>
    </div>
    <div class="category-score">
    Score: <input type="text" class="category-score-input" onchange="updateScore(event, 'formal')" value="${typeof student.categories[0].score === 'number' ? student.categories[0].score : ''}" onkeyup="checkExpandCategory(event, 'formal')" /> / 10
    <span id="formal-grade" class="category-grade"> ( ${typeof student.categories[0].score === 'number' ? Math.round(student.categories[0].score / student.categories[0].maxScore * 100) / 10 : '...'} / 10)</span>
    </div>
    <div class="category-content" id="category-content-formal">
      <div class="category-content-item">
        Is the following provided: name, student number, course, group, date?
        <textarea onblur="updateComment(event, { category: 'formal', index: 0 })">${student.categories[0].comments[0] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay have a bibliography?
        <textarea onblur="updateComment(event, { category: 'formal', index: 1 })">${student.categories[0].comments[1] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay have a title?
        <textarea onblur="updateComment(event, { category: 'formal', index: 2 })">${student.categories[0].comments[2] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay not exceed the word limit?
        <textarea onblur="updateComment(event, { category: 'formal', index: 3 })">${student.categories[0].comments[3] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay address <i>all</i> questions and sub questions?
        <textarea onblur="updateComment(event, { category: 'formal', index: 4 })">${student.categories[0].comments[4] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay use <i>all</i> tutorial literature?
        <textarea onblur="updateComment(event, { category: 'formal', index: 5 })">${student.categories[0].comments[5] || ''}</textarea>
      </div>
    </div>
  </div>
  <div class="category">
    <input type="radio" name="toggle-category" id="organization" class="toggle-radio" />
    <div class="category-header" id="cat-header-organization" onmousedown="verifyRadioButtonAction('organization')" onclick="resetScroll('organization')">
      <label for="organization" class="label">
        Organization and structure
      </label>
    </div>
    <div class="category-score">
    Score: <input type="text" class="category-score-input" onchange="updateScore(event, 'organization')" value="${typeof student.categories[1].score === 'number' ? student.categories[1].score : ''}" onkeyup="checkExpandCategory(event, 'organization')" /> / 15
    <span id="organization-grade" class="category-grade"> ( ${typeof student.categories[1].score === 'number' ? Math.round(student.categories[1].score / student.categories[1].maxScore * 100) / 10 : '...'} / 10)</span>
    </div>
    <div class="category-content" id="category-content-organization">
    <i>Aim/direction: obvious, clear, and functional</i>
      <div class="category-content-item">
        Is the title appropriate? That is, does it reflect the content of the essay?
        <textarea onblur="updateComment(event, { category: 'organization', index: 0 })">${student.categories[1].comments[0] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Do you have a precise idea of what the essay will be about based on the introduction only?
        <textarea onblur="updateComment(event, { category: 'organization', index: 1 })">${student.categories[1].comments[1] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the conclusion restate the central thesis of the essay and does it summarize how the essay argued for that thesis?
        <textarea onblur="updateComment(event, { category: 'organization', index: 2 })">${student.categories[1].comments[2] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Is it clear for each paragraph how it contributes to the overall thesis of the essay? Are there no paragraphs that are redundant and can be removed?
        <textarea onblur="updateComment(event, { category: 'organization', index: 3 })">${student.categories[1].comments[3] || ''}</textarea>
      </div>
      <br>
      <i>Structure</i>
      <div class="category-content-item">
        Is the essay clearly and consistently divided into paragraphs?
        <textarea onblur="updateComment(event, { category: 'organization', index: 4 })">${student.categories[1].comments[4] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does each paragraph focus on at most one idea, concept, or argument?
        <textarea onblur="updateComment(event, { category: 'organization', index: 5 })">${student.categories[1].comments[5] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Is the structure of the essay visually clear (by using e.g. headers, sections, and clear paragraph dividers)?
        <textarea onblur="updateComment(event, { category: 'organization', index: 6 })">${student.categories[1].comments[6] || ''}</textarea>
      </div>
      <br>
      <i>Redundancy</i>
      <div class="category-content-item">
        Are there no redundant sentences or explanations?
        <textarea onblur="updateComment(event, { category: 'organization', index: 7 })">${student.categories[1].comments[7] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Is the amount of attention paid to concepts, arguments, or ideas in line with their importance for the overall thesis of the essay? That is, are e.g. concepts of minor importance not too elaborately explained or major concepts hardly explained?
        <textarea onblur="updateComment(event, { category: 'organization', index: 8 })">${student.categories[1].comments[8] || ''}</textarea>
      </div>
    </div>
  </div>
  <div class="category">
    <input type="radio" name="toggle-category" id="representation" class="toggle-radio" />
    <div class="category-header" id="cat-header-representation" onmousedown="verifyRadioButtonAction('representation')" onclick="resetScroll('representation')">
      <label for="representation" class="label">
        Representation and explanation
      </label>
    </div>
    <div class="category-score">
    Score: <input type="text" class="category-score-input" onchange="updateScore(event, 'representation')" value="${typeof student.categories[2].score === 'number' ? student.categories[2].score : ''}" onkeyup="checkExpandCategory(event, 'representation')" /> / 20
    <span id="representation-grade" class="category-grade"> ( ${typeof student.categories[2].score === 'number' ? Math.round(student.categories[2].score / student.categories[2].maxScore * 100) / 10 : '...'} / 10)</span>
    </div>
    <div class="category-content" id="category-content-representation">
      <div class="category-content-item">
        Are the views of others (most notably the tutorial literature) represented correctly and accurately? Are all relevant concepts and arguments from those papers there?
        <textarea onblur="updateComment(event, { category: 'representation', index: 0 })">${student.categories[2].comments[0] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay attempt to present a position in its strongest form before criticizing that position?
        <textarea onblur="updateComment(event, { category: 'representation', index: 1 })">${student.categories[2].comments[1] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Are the core concepts defined and explained in a precise and unambiguous way?
        <textarea onblur="updateComment(event, { category: 'representation', index: 2 })">${student.categories[2].comments[2] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Are (technical) concepts and terms correctly and consistently applied?
        <textarea onblur="updateComment(event, { category: 'representation', index: 3 })">${student.categories[2].comments[3] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Are abstract concepts illustrated with examples?
        <textarea onblur="updateComment(event, { category: 'representation', index: 4 })">${student.categories[2].comments[4] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Are the required references there?
        <textarea onblur="updateComment(event, { category: 'representation', index: 5 })">${student.categories[2].comments[5] || ''}</textarea>
      </div>
    </div>
  </div>
  <div class="category">
    <input type="radio" name="toggle-category" id="argumentation" class="toggle-radio" />
    <div class="category-header" id="cat-header-argumentation" onmousedown="verifyRadioButtonAction('argumentation')" onclick="resetScroll('argumentation')">
      <label for="argumentation" class="label">
        Argumentation and reasoning
      </label>
    </div>
    <div class="category-score">
    Score: <input type="text" class="category-score-input" onchange="updateScore(event, 'argumentation')" value="${typeof student.categories[3].score === 'number' ? student.categories[3].score : ''}" onkeyup="checkExpandCategory(event, 'argumentation')" /> / 30
    <span id="argumentation-grade" class="category-grade"> ( ${typeof student.categories[3].score === 'number' ? Math.round(student.categories[3].score / student.categories[3].maxScore * 100) / 10 : '...'} / 10)</span>
    </div>
    <div class="category-content" id="category-content-argumentation">
      <div class="category-content-item">
        Does the essay not contradict itself?
        <textarea onblur="updateComment(event, { category: 'argumentation', index: 0 })">${student.categories[3].comments[0] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay avoid obviously faulty arguments, such as those that (i) argue “in a circle”, (ii) rely on an ambiguity, (iii) support a conclusion other than the officially stated conclusion?
        <textarea onblur="updateComment(event, { category: 'argumentation', index: 1 })">${student.categories[3].comments[1] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Is the structure of the arguments clear? That is, is it obvious which premises belong to the which argument and how the conclusion follows from those premises?
        <textarea onblur="updateComment(event, { category: 'argumentation', index: 2 })">${student.categories[3].comments[2] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Are (core) claims and arguments not based on opinion ('I find…', 'I believe…')?
        <textarea onblur="updateComment(event, { category: 'argumentation', index: 3 })">${student.categories[3].comments[3] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay argue for its thesis in detail? That is, does it provide good reasons for believing its thesis and does it explain why those are good reasons?
        <textarea onblur="updateComment(event, { category: 'argumentation', index: 4 })">${student.categories[3].comments[4] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay offer thoughtful insights, rather than use the examples and arguments in the literature unthinkingly?
        <textarea onblur="updateComment(event, { category: 'argumentation', index: 5 })">${student.categories[3].comments[5] || ''}</textarea>
      </div>
    </div>
  </div>
  <div class="category">
    <input type="radio" name="toggle-category" id="language" class="toggle-radio" />
    <div class="category-header" id="cat-header-language" onmousedown="verifyRadioButtonAction('language')" onclick="resetScroll('language')">
      <label for="language" class="label">
        Language and style
      </label>
    </div>
    <div class="category-score">
    Score: <input type="text" class="category-score-input" onchange="updateScore(event, 'language')" value="${typeof student.categories[4].score === 'number' ? student.categories[4].score : ''}" onkeyup="checkExpandCategory(event, 'language')" /> / 10
    <span id="language-grade" class="category-grade"> ( ${typeof student.categories[4].score === 'number' ? Math.round(student.categories[4].score / student.categories[4].maxScore * 100) / 10 : '...'} / 10)</span>
    </div>
    <div class="category-content" id="category-content-language">
      <div class="category-content-item">
        Is grammar correctly applied?
        <textarea onblur="updateComment(event, { category: 'language', index: 0 })">${student.categories[4].comments[0] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Is the writing academic? That is, does it not use popular forms such ain\'t, I\'ve, I\'m gonna?
        <textarea onblur="updateComment(event, { category: 'language', index: 1 })">${student.categories[4].comments[1] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Is the essay easy to understand? E.g., are there no long and difficult to understand sentences or vague pieces of text?
        <textarea onblur="updateComment(event, { category: 'language', index: 2 })">${student.categories[4].comments[2] || ''}</textarea>
      </div>
  </div>
  <div class="category">
    <input type="radio" name="toggle-category" id="beyond" class="toggle-radio" />
    <div class="category-header" id="cat-header-beyond" onmousedown="verifyRadioButtonAction('beyond')" onclick="resetScroll('beyond')">
      <label for="beyond" class="label">
        Diving deeper
      </label>
    </div>
    <div class="category-score">
    Score: <input type="text" class="category-score-input" onchange="updateScore(event, 'beyond')" value="${typeof student.categories[5].score === 'number' ? student.categories[5].score : ''}" onkeyup="checkExpandCategory(event, 'beyond')" /> / 15
    <span id="beyond-grade" class="category-grade"> ( ${typeof student.categories[5].score === 'number' ? Math.round(student.categories[5].score / student.categories[5].maxScore * 100) / 10 : '...'} / 10)</span>
    </div>
    <div class="category-content" id="category-content-beyond">
      <div class="category-content-item">
        Are connections, complementarities, or disagreements between the tutorial literature discussed and used in the argumentation?
        <textarea onblur="updateComment(event, { category: 'beyond', index: 0 })">${student.categories[5].comments[0] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Are sources other than the tutorial literature used in the argumentation?
        <textarea onblur="updateComment(event, { category: 'beyond', index: 1 })">${student.categories[5].comments[1] || ''}</textarea>
      </div>
      <div class="category-content-item">
        Does the essay develop an own contribution, where *own* means not (only) based on the tutorial literature or other sources? This could be in the form of an argument, a conceptual distinction, or something else that adds value to the essay.
        <textarea onblur="updateComment(event, { category: 'beyond', index: 2 })">${student.categories[5].comments[2] || ''}</textarea>
      </div>
  </div>
  `;

  contentHeader.innerHTML = `${student.name}<br><span class="print-student" onclick="printResultsOfSelectedStudent()">Print feedback for ${student.name}</span>`;
  contentForm.innerHTML = innerHTML;
}

function verifyRadioButtonAction(category) {
  /* Checks whether we need to collapse or decollapse a category */
  const target = document.getElementById(category);
  const targetIsExpanded = target.checked;

  const handleClick = function () {
    target.checked = false;
    target.removeEventListener('click', handleClick, true);
  }

  if (targetIsExpanded) {
    target.checked = false;
    target.addEventListener('click', handleClick, true);
  }
}

function checkExpandCategory({ code: key }, category) {
  /* Checks whether we need to collapse or decollapse a category and scroll into view */
  if (key === 'Tab') {
    document.getElementById(category).checked = true;
    resetScroll(category);
  }
} 

function resetScroll(category) {
  const scrollUnit = window.outerHeight / 10;
  let scrollTo = 0;
  if (category === 'formal') scrollTo = 0;
  else if (category === 'organization') scrollTo = scrollUnit;
  else if (category === 'representation') scrollTo = 2 * scrollUnit;
  else if (category === 'argumentation') scrollTo = 3 * scrollUnit;
  else if (category === 'language') scrollTo = 4 * scrollUnit;
  else if (category === 'beyond') scrollTo = 5 * scrollUnit;

  document.getElementById('content-form').scrollTop = scrollTo;
}

/* Save, export, import */
function saveToLocalStorage(backup) {
  try {
    const toStore = {};
    data.groups.forEach((group) => {
      toStore[group.name] = [];
      group.students.forEach((student) => {
        toStore[group.name].push({
          name: student.name,
          categories: student.categories,
          generalComment: student.generalComment,
          grade: student.grade,
        });
      });
    });
  
   localStorage.setItem('dataessay2020', JSON.stringify(toStore));
    if (backup) localStorage.setItem('backup-dataessay2020', JSON.stringify(toStore));
  }
  catch(err) {
    showError(err);
  }
}

function printAll() {
  const w = window.open();
  let bodyHTML = '';
  data.groups.forEach((group) => {
    bodyHTML += `<h1 style="margin: 0;">Group: ${group.name}</h1>`;
    bodyHTML += `<i>Number of students: ${group.students.length}</i><br>`;
    bodyHTML += `<i>Average grade: ${group.stats.averageGrade}</i><br><br>`;
    bodyHTML += `<i>Percentage pass: ${group.stats.percentagePass}%</i><br><br>`;

    group.students.forEach((student) => {
      bodyHTML += getPrintContentPerStudent(student);
    });

  });
  w.document.write(`<html><head><title>Essay grading results</title><style>body { font-family: 'Open Sans', 'Verdana'; padding: 2rem; }</style></head><body>${bodyHTML}</body></html>`);
  w.window.print();
  w.window.close();
  return false;
}

function printResultsOfSelectedStudent() {
  const w = window.open();
  let bodyHTML = '';

  bodyHTML += getPrintContentPerStudent(data.selectedStudent, true);

  w.document.write(`<html><head><title>Essay feedback ${data.selectedStudent.name}</title><style>body { font-family: 'Open Sans', 'Verdana'; padding: 2rem; }</style></head><body>${bodyHTML}</body></html>`);
  w.window.print();
  w.window.close();
  return false;
}

function getPrintContentPerStudent(student, isIndividualPrint) {
  let printContent = '';
  printContent += `<b>${student.name}</b><br>`;
  if (isIndividualPrint) {
    printContent += `<i style="font-size: 0.9rem;">Essay feedback - Philosophy, block 4, ESE, Erasmus University Rotterdam</i><br><br>`;
    printContent += `<span style="width: 12vw; display: flex; justify-content: space-between;">Grade <b>${student.grade || '-'}</b></span><br><br>`;
  }
  else printContent += `<i>Grade: ${student.grade || '-'}</i><br><br>`;
  printContent += `<div style="font-size: 0.85rem; white-space: pre-line;">`;
  printContent += `<u>General comment</u><br>`;
  printContent += `${student.generalComment || '-'}<br><br>`;

  student.categories.forEach((category) => {
    printContent += `<u>${category.name.toUpperCase()}</u><br>`;
    printContent += `<i>Score: ${category.score || '-'} / ${category.maxScore}</i><br><br>`;
    const categoryCriteria = criteria[category.name];
    categoryCriteria.forEach((criterium, index) => {
      printContent += `<i>${criterium}</i>`;
      printContent += `<div style="padding: 0.5rem; white-space: pre-line;">${category.comments[index] || '-'}</div><br>`;
    });
    printContent += ``;
    printContent += ``;
  });
  printContent += ``;
  printContent += ``;
  printContent += `</div><br><br>`;

  return printContent;
}

function exportToCsv() {
  try {
    let csvContent = 'data:text/csv;charset=utf-8,';
  
    // Add headers for the columns
    // (!) If you change any of this, make sure that the hardcoded array indexes used in the importFromCsv function are changed accordingly!
    const columns = [
      'Group', 'Student', 'Grade', 'General comment', 'Formal (score / 10)', 'Organization (score / 15)', 'Representation (score / 20)', 'Argumentation (score / 30)', 'Language (score / 10)', 'Diving deeper (score / 15)', 'Formal (grade 1-10)', 'Organization (grade 1-10)', 'Representation (grade 1-10)', 'Argumentation (grade 1-10)', 'Language (grade 1-10)', 'Diving deeper (grade 1-10)',
      '|', 'Is the following provided: name, student, number, course, group, date?', 'Does the essay have a bibliography?', 'Does the essay have a title?', 'Does the essay not exceed the word limit?', 'Does the essay address all questions and sub questions?', 'Does the essay use all tutorial literature?',
      '|', 'Is the title appropriate? That is, does it reflect the content of the essay?', 'Do you have a precise idea of what the essay will be about based on the introduction only?', 'Does the conclusion restate the central thesis of the essay, and does it summarize how the essay argued for that thesis?', 'Is it clear for each paragraph how it contributes to the overall thesis of the essay? Are there no paragraphs that are redundant and can be removed?', 'Is the essay clearly and consistently divided into paragraphs?', 'Does each paragraph focus on at most one idea concept argument?', 'Is the structure of the essay visually clear (by using e.g. headers, sections, and clear paragraph dividers)?', 'Are there no redundant sentences or explanations?', 'Is the amount of attention paid to concepts arguments or ideas in line with their importance for the overall thesis of the essay? That is, are e.g. concepts of minor importance not too elaborately explained or major concepts hardly explained?',
      '|', 'Are the views of others (most notably the tutorial literature) represented correctly and accurately? Are all relevant concepts and arguments from those papers there?', 'Does the essay attempt to present a position in its strongest form before criticizing that position?', 'Are the core concepts defined and explained in a precise and unambiguous way?', 'Are (technical) concepts and terms correctly and consistently applied?', 'Are abstract concepts illustrated with examples?', 'Are the required references there?',
      '|', 'Does the essay not contradict itself?', 'Does the essay avoid obviously faulty arguments such as those that (i) argue in a circle, (ii) rely on an ambiguity, (iii) support a conclusion other than the officially stated conclusion?', 'Is the structure of the arguments clear? That is, is it obvious which premises belong to the which argument and how the conclusion follows from those premises?', 'Are (core) claims and arguments not based on opinion (I find…, I believe…)?', 'Does the essay argue for its thesis in detail? That is, does it provide good reasons for believing its thesis and does it explain why those are good reasons?', 'Does the essay offer thoughtful insights rather than use the examples and arguments in the literature unthinkingly?',
      '|', 'Is grammar correctly applied?', 'Is the writing academic? That is, does it not use popular forms such as ain\'t, I\'ve, I\'m gonna?', 'Is the essay easy to understand? E.g., are there no long and difficult to understand sentences or vague pieces of text?',
      '|', 'Are connections, complementarities, or disagreements between the tutorial literature discussed and used in the argumentation?', 'Are sources other than the tutorial literature used in the argumentation?', 'Does the essay develop an own contribution, where *own* means not (only) based on the tutorial literature or other sources? This could be in the form of an argument, a conceptual distinction, or something else that adds value to the essay.'
    ];
    csvContent = csvContent + '"' + columns.join('","') + '"\n';
  
    // Add data
    data.groups.forEach((group) => {
      group.students.forEach((student) => {
        const dataRow = [];
        dataRow.push(
          group.name,
          student.name,
          typeof student.grade === 'number' ? student.grade : '',
          student.generalComment || '',
          typeof student.categories[0].score === 'number' ? student.categories[0].score : '',
          typeof student.categories[1].score === 'number' ? student.categories[1].score : '',
          typeof student.categories[2].score === 'number' ? student.categories[2].score : '',
          typeof student.categories[3].score === 'number' ? student.categories[3].score : '',
          typeof student.categories[4].score === 'number' ? student.categories[4].score : '',
          typeof student.categories[5].score === 'number' ? student.categories[5].score : '',
          typeof student.categories[0].score === 'number' ? Math.round((student.categories[0].score / student.categories[0].maxScore) * 100) / 10 : '',
          typeof student.categories[1].score === 'number' ? Math.round((student.categories[1].score / student.categories[1].maxScore) * 100) / 10 : '',
          typeof student.categories[2].score === 'number' ? Math.round((student.categories[2].score / student.categories[2].maxScore) * 100) / 10 : '',
          typeof student.categories[3].score === 'number' ? Math.round((student.categories[3].score / student.categories[3].maxScore) * 100) / 10 : '',
          typeof student.categories[4].score === 'number' ? Math.round((student.categories[4].score / student.categories[4].maxScore) * 100) / 10 : '',
          typeof student.categories[5].score === 'number' ? Math.round((student.categories[5].score / student.categories[5].maxScore) * 100) / 10 : ''
        );
  
        // Comments per criterium. Number of criteria per category needs to be hardcoded since the number of comments for a student for a category may not correspond with the number of criteria we have
        student.categories.forEach((category) => {
          let numberOfCriteria = 5; // 0-based; 5 for 'formal', 'representation', and 'argumentation'
          if (category.name === 'organization') numberOfCriteria = 8;
          else if (category.name === 'language' || category.name === 'beyond') numberOfCriteria = 2;
  
          // First an empty cell for a 'divider' column
          dataRow.push('|');
  
          // The comments for this category
          for (i = 0; i <= numberOfCriteria; i++) {
            dataRow.push(category.comments[i] || '');
          }
        });
  
        csvContent = csvContent + '"' + dataRow.join('","') + '"\n';
      });
    });
  
  
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement('a');
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', 'Essay grading results.csv');
    document.body.appendChild(link);
    link.click();
  
    hideGeneralModal();
    localStorage.setItem('gradedessayssincelastexport', 0);
  }
  catch(err) {
    showError(err);
  }
}

function handleFileImport({ target }) {
  try {
    const file = target.files[0];
    if (!file) return;
  
    const { name } = file;
  
    if (name.substring(name.length - 3) !== 'csv') {
      alert('Please select a valid csv file!');
      return;
    }
  
    Papa.parse(file, {
      dynamicTyping: true,
      complete: function(results) {
        target.value = null;
        importFromCsv(results.data);
      }
    });
  }
  catch(err) {
    showError(err);
  }
}

function importFromCsv(importData) {
  try {
    // Store selectedGroup, so we can set it back later
    const initialSelectedGroup = data.selectedGroup && data.selectedGroup.name;
  
    // Cleanup data (get rid of headers)
    importData.splice(0, 1);
  
    // Restore groups
    let groups = importData.map(item => item[0]);
    groups = [...new Set(groups)];
    groups = groups.filter(item => !!item);
    data.restoreGroups(groups);

    if (restoreError) {
      restoreError = false;
      return;
    }
  
    let currentlyProcessingGroupName = null;
    const studentObjects = [];
    importData.forEach((student, index) => {
      if (!student[0] || student[0] === '') return; /* the parser gives null values for last rows sometimes */
  
      const studentObj = {
        name: student[1],
        grade: student[2],
        generalComment: student[3],
        categories: [],
      };
  
      // helperData item contains 1) category, 2) maxScore, 3) index for 'score', 4 & 5) range of indexes for 'comments'
      const helperData = [
        ['formal', 10, 4, 17, 22],
        ['organization', 15, 5, 24, 32],
        ['representation', 20, 6, 34, 39],
        ['argumentation', 30, 7, 41, 46],
        ['language', 10, 8, 48, 50],
        ['beyond', 15, 9, 52, 54],
      ];
      for (i=0; i<6; i++) { // Go over all categories
        const categoriesObj = {
          name: helperData[i][0],
          maxScore: helperData[i][1],
          score: student[helperData[i][2]],
          comments: [],
        }
        for (j = helperData[i][3]; j <= helperData[i][4]; j++) { // Copy all comments for this category
          categoriesObj.comments.push(student[j]);
        }
        studentObj.categories.push(categoriesObj);
      }
  
      if (!currentlyProcessingGroupName) {
        // First student
        data.selectedGroup = student[0];
        currentlyProcessingGroupName = student[0];
  
        studentObjects.push(studentObj);
      }
      else if (currentlyProcessingGroupName !== student[0]) {
        // Student from a different group than we had until now
        data.selectedGroup.restoreStudents(studentObjects);
        studentObjects.length = 0;
        currentlyProcessingGroupName = student[0];
        data.selectedGroup = student[0];
  
        studentObjects.push(studentObj);
      }
      else if (index === importData.length - 2) { // -2 instead of -1 to account for the last row which only contains a null value
        // Last student
        studentObjects.push(studentObj);
  
        data.selectedGroup.restoreStudents(studentObjects);
        studentObjects.length = 0;
        if (initialSelectedGroup) data.selectedGroup = initialSelectedGroup;
        else data.selectedGroup = groups[0];
      }
      else studentObjects.push(studentObj);
  
    });
  
    if (restoreError) restoreError = false; // Reset
    else saveToLocalStorage();
  }
  catch(err) {
    showError(err);
  }
}

/* Other, modals */
function toggleDisplayAddGroupModal() {
  const modalIsActive = modalNewGroup.style.display === 'block';
  const hasGroups = !!data.groups.length;
  if (modalIsActive) {
    modalNewGroup.style.display = 'none';
    overlay.style.display = 'none';
  }
  else {
    modalNewGroup.style.display = 'block';
    overlay.style.display = 'block';
    if (hasGroups) modalNewGroupMessage.innerHTML = 'Enter a name for the new group:';
    else modalNewGroupMessage.innerHTML = 'Start by entering a name for your first group:';
    modalNewGroupInput.focus();
  }
}

function checkDisplayExportReminder() {
  let gradedEssaysSinceLastExport = localStorage.getItem('gradedessayssincelastexport');
  if (!gradedEssaysSinceLastExport) gradedEssaysSinceLastExport = 0;

  gradedEssaysSinceLastExport++;
  localStorage.setItem('gradedessayssincelastexport', gradedEssaysSinceLastExport);

  const gradedEssaysIsMultipleOfFive = (gradedEssaysSinceLastExport / 5) === Math.round(gradedEssaysSinceLastExport / 5);
  if (gradedEssaysIsMultipleOfFive) displayExportReminder(gradedEssaysSinceLastExport);
}

function displayExportReminder(gradedEssaysSinceLastExport) {
  modalBoxGeneral.innerHTML = `
<div style="font-size: 0.9rem;">
Nice, another essay done!<br><br>
The last ${gradedEssaysSinceLastExport} essays you graded have not been exported yet. Export now?<br><br>
<button onclick="hideGeneralModal()" class="custom-button">Cancel</button>
<button onclick="exportToCsv()" class="custom-button" style="background: rgb(0, 80, 0); color: white;">Export to CSV</button>
</div>
  `;

  modalGeneral.style.display = 'block';
  overlay.style.display = 'block';
}

function hideGeneralModal() {
  modalGeneral.style.display = 'none';
  overlay.style.display = 'none';
}

function showError(err) {
  overlay.style.display = 'block';
  modalGeneral.style.display = 'block';
  modalBoxGeneral.innerHTML = `
Oops, an error occurred!<br>
Please send the error below to Anne Albert:<br><br>

<pre style="text-align: left;">${err}<br>${err.stack}</pre>
<br><br>
<button class="custom-button" onclick="hideGeneralModal()">Dismiss</button>
  `;

  console.error(err);
}

function showNotification({ message, duration }) {
  notification.innerText = message;
  notification.style.opacity = 1;
  setTimeout(() => {
    notification.style.opacity = 0;
  }, duration);
}

/* Restore ackup */
function restoreBackup() {
  try {
    let localData = localStorage.getItem('backup-dataessay2020');
    if (localData) localData = JSON.parse(localData);
    else {
      alert('No backup found!');
      return;
    }

    if (!confirm('This restores your data as it was when you last added a comment or a score. Continue?')) return;

    restoreFromLocalStorage(localData);
  }
  catch(err) {
    showError(err);
  }
}

function restoreFromLocalStorage(dataToRestore) {
  try {
    /* Restore locally stored data */
    const groupNames = Object.keys(dataToRestore);
    data.restoreGroups(groupNames);
  
    if (restoreError) {
      restoreError = false; // Reset
      return;
    }

    groupNames.forEach((groupName) => {
      const groupRef = data.groups.find((group) => group.name === groupName);
      groupRef.restoreStudents(dataToRestore[groupName]);
      data.selectedGroup = groupName;
    });

    if (restoreError) {
      restoreError = false; // Reset
      return;
    }

    data.selectedGroup = groupNames[0];
    data.selectedGroup.calculateAverageGrade();
    data.populateGroupView();

    saveToLocalStorage();
  }
  catch(err) {
    showError(err);
  }
}

/*
Check whether there has been an update, and display info message if so.
Whenever there's an 'important' update, add an info message to the message array.
*/
function checkUpdateMessage() {
  let dismissedUpdates = localStorage.getItem('dismissedUpdatesB5');
  if (!dismissedUpdates) dismissedUpdates = 0;

  const updateMessages = [
`
<span style="font-size: 0.9rem;">
<b>Update block 5</b><br><br>
<ul>
<li>The maximum scores for the categories is adapted (30 for argumentation, 15 for diving deeper)</li>
</ul>
<br><br>
<button class="custom-button" onclick="processUpdateMessageDismissal()">Cool!</button>
</span>
`,
`
<span style="font-size: 0.9rem;">
<b>Update</b><br><br>
<ul>
<li>Saved comments are now copied without leading or trailing whitespaces</li>
</ul>
<br><br>
<button class="custom-button" onclick="processUpdateMessageDismissal()">Cool!</button>
</span>
`,
  ];

  if (dismissedUpdates < updateMessages.length) {
    modalBoxGeneral.innerHTML = updateMessages[dismissedUpdates];

    overlay.style.display = 'block';
    modalGeneral.style.display = 'block';
    modalBoxGeneral.style.width = '30vw';
  }
}

function processUpdateMessageDismissal() {
  let dismissedUpdates = localStorage.getItem('dismissedUpdatesB5');
  if (!dismissedUpdates) dismissedUpdates = 0;
  dismissedUpdates++;
  localStorage.setItem('dismissedUpdatesB5', dismissedUpdates);

  if (modalNewGroup.style.display !== 'block') overlay.style.display = 'none';
  modalGeneral.style.display = 'none';
  modalBoxGeneral.style.width = '25vw'; // default value in style.css

  checkUpdateMessage();
}

/* Grading criteria (used for printing functions) */
const criteria = {
  'formal': ['Is the following provided: name, student number, course, group, date?', 'Does the essay have a bibliography?', 'Does the essay have a title?', 'Does the essay not exceed the word limit?', 'Does the essay address <i>all</i> questions and sub questions?', 'Does the essay use <i>all</i> tutorial literature?'],
  'organization': ['Is the title appropriate? That is, does it reflect the content of the essay?', 'Do you have a precise idea of what the essay will be about based on the introduction only?', 'Does the conclusion restate the central thesis of the essay and does it summarize how the essay argued for that thesis?', 'Is it clear for each paragraph how it contributes to the overall thesis of the essay? Are there no paragraphs that are redundant and can be removed?', 'Is the essay clearly and consistently divided into paragraphs?', 'Does each paragraph focus on at most one idea, concept, or argument?', 'Is the structure of the essay visually clear (by using e.g. headers, sections, and clear paragraph dividers)?', 'Are there no redundant sentences or explanations?', 'Is the amount of attention paid to concepts, arguments, or ideas in line with their importance for the overall thesis of the essay? That is, are e.g. concepts of minor importance not too elaborately explained or major concepts hardly explained?'],
  'representation': ['Are the views of others (most notably the tutorial literature) represented correctly and accurately? Are all relevant concepts and arguments from those papers there?', 'Does the essay attempt to present a position in its strongest form before criticizing that position?', 'Are the core concepts defined and explained in a precise and unambiguous way?', 'Are (technical) concepts and terms correctly and consistently applied?', 'Are abstract concepts illustrated with examples?', 'Are the required references there?'],
  'argumentation': ['Does the essay not contradict itself?', 'Does the essay avoid obviously faulty arguments, such as those that (i) argue “in a circle”, (ii) rely on an ambiguity, (iii) support a conclusion other than the officially stated conclusion?', 'Is the structure of the arguments clear? That is, is it obvious which premises belong to the which argument and how the conclusion follows from those premises?', 'Are (core) claims and arguments not based on opinion (\'I find…\', \'I believe…\')?', 'Does the essay argue for its thesis in detail? That is, does it provide good reasons for believing its thesis and does it explain why those are good reasons?', 'Does the essay offer thoughtful insights, rather than use the examples and arguments in the literature unthinkingly?'],
  'language': ['Is grammar correctly applied?', 'Is the writing academic? That is, does it not use popular forms such as ain\'t, I\'ve, I\'m gonna?', 'Is the essay easy to understand? E.g., are there no long and difficult to understand sentences or vague pieces of text?'],
  'beyond': ['Are connections, complementarities, or disagreements between the tutorial literature discussed and used in the argumentation?', 'Are sources other than the tutorial literature used in the argumentation?', 'Does the essay develop an own contribution, where *own* means not (only) based on the tutorial literature or other sources? This could be in the form of an argument, a conceptual distinction, or something else that adds value to the essay.'],
};

/* Setup on load (get DOM refs, restore locally stored data or create new data objects) */
const container = document.getElementById('container');
const modalNewGroup = document.getElementById('modal-new-group');
const modalNewGroupMessage = document.getElementById('modal-new-group-message');
const modalNewGroupInput = document.getElementById('modal-new-group-input');
const modalGeneral = document.getElementById('modal-general');
const modalBoxGeneral = document.getElementById('modal-box-general');
modalNewGroupInput.addEventListener('keyup', (event) => {
  if (event.keyCode === 13) createGroup();
});
const overlay = document.getElementById('overlay');
const selectGroup = document.getElementById('select-group');
const graded = document.getElementById('graded');
const average = document.getElementById('average');
// const averageIncludingZeroes = document.getElementById('average-including-zeroes');
const percentagePass = document.getElementById('percentage-pass');
const addStudentInput = document.getElementById('add-student-input');
const addStudentList = document.getElementById('add-student-list');
addStudentInput.addEventListener('keyup', (event) => {
  if (event.keyCode === 13) addStudent();
});
const studentsList = document.getElementById('students-list');
const contentHeader = document.getElementById('content-header');
const contentForm = document.getElementById('content-form');
const savedCommentsContainer = document.getElementById('saved-comments-container');
const savedComments = document.getElementById('saved-comments');
const newComment = document.getElementById('new-saved-comment');
const newCommentTitle = document.getElementById('new-comment-title');
const newCommentBody = document.getElementById('new-comment-body');
const addNewSavedCommentButton = document.getElementById('add-saved-comment-button');
const copyBox = document.getElementById('copy-box');
const notification = document.getElementById('notification');
populateSavedComments();

let restoreError = false; // Used to determine whether to saveToLocalStorage(), depending on whether there's an error in restoreGroups or restoreStudents

const data = getDraftDataObject();
let localData = localStorage.getItem('dataessay2020');
if (localData) localData = JSON.parse(localData);
if (localData && Object.keys(localData).length) restoreFromLocalStorage(localData);
else {
  /* No locally stored data yet. Show modal asking for the first group */
  contentHeader.innerHTML = 'Create a group first!';
  modalNewGroupMessage.innerHTML = 'Start by entering a name for your first group:';
  toggleDisplayAddGroupModal();
}

checkUpdateMessage();
